import React from 'react';
import Header from '../Layout/Header/index';
import Footer from '../Layout/Footer/index';
import Theme from '../theme';
export default function Index(props) {
    const {children} = props
  return(
    <div>
      <>
        <Theme/>
      </>
        <Header/>
        <main>{children}</main>
        <Footer/>
    </div>
  );
}
