import React from 'react';
import styles from './Footer.module.scss';
const TeqquesLogo = "/assets/logo/footer-logo.svg";
export default function index() {
  return(
    <div>
        <div className='container'>
          <div className={styles.footer}>
            <div className={styles.grid}>
              <div className={styles.gridItems}>
                  <h2>
                  {"We'd love to hear from you.Let’s make something amazing together"}<span> Hello@teqqed.com</span>
                  </h2>
              </div>
              <div className={styles.gridItems}>
                <div className={styles.menuGrid}>
                  <div className={styles.menuGridItems}>
                    <p>Menu</p>
                    <ul>
                      <li>About us</li>
                      <li>Works</li>
                      <li>Careers</li>
                      <li>Insight</li>
                    </ul>
                  </div>
                  <div className={styles.menuGridItems}>
                    <p>Social media</p>
                    <ul>
                      <li>Facebook</li>
                      <li>Instagram</li>
                      <li>Dribbble</li>
                      <li>Bēhance</li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
            <div className={styles.copyRight}>
              <div>
                <img src={TeqquesLogo} alt="TeqquesLogo"/>
              </div>
              <div>
                <ul>
                  <li>{"© 2015–2022 teqqed"}</li>
                  <li>Privacy Policy</li>
                </ul>
              </div>
            </div>
          </div>
        </div>
    </div>
  );
}
