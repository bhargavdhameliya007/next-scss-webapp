import React ,{ useEffect, useState } from 'react';
import styles from './header.module.scss';
import {mode} from '../../Jotai/jotai'
import { useAtom } from 'jotai';

const TeqquesLogo = "/assets/logo/teqqed.svg";
const LightTeqquesLogo = "/assets/logo/light-teqqed-logo.svg";
const MenuIcon = "/assets/icons/menu-icon.svg";

export default function Header() {
  const [theme,settheme] = useAtom(mode);



  return(
    <div>
      <div className='container-fluid'>
        <div className={styles.header}>
          <div className={styles.headerAlignment}>
            <div className={styles.logo}>
              <img src={theme==='dark'? TeqquesLogo: LightTeqquesLogo} alt="TeqquesLogo"/>
            </div>
            <div className={styles.menu}>
              <nav>
                <ul>
                  <li>Work<sup>04</sup></li>
                  <li>Capabilities</li>
                  <li>About</li>
                  <li>Careers<sup>03</sup></li>
                </ul>
              </nav>
            </div>
            <div className={styles.getInTouch}>
              <span>Get in touch</span>
            </div>
            <div className={styles.mobileView}>
              <img src={MenuIcon} alt="MenuIcon"/>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
