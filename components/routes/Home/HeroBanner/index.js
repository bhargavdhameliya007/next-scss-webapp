import React from 'react'
import styles from './hero-banner.module.scss';
export default function HeroBanner() {
  return (
    <div>
        <div className={styles.heroBanner}>
            <div className='container'>
                <div className={styles.heroText}>
                    <h1>
                        Digital product design 
                        + <span>development for 
                        winning</span> brands and companies.
                    </h1>
                    <p>
                        Dutch digital agency designing and building high-
                        end digital products for ambitious fintech, medtech and real estate companies.
                    </p>
                </div>
            </div>
        </div>
    </div>
  )
}
