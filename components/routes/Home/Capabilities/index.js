import React from 'react'
import styles from './capabilities.module.scss';
const StrategyIcon = "/assets/icons/strategy.svg";
const DesignIcon = "/assets/icons/design.svg";
const DevlopmentIcon = "/assets/icons/devlopment.svg";
export default function Capabilities() {
  return (
    <div>
        <div className={styles.CapabilitiesAlignment}>
            <div className="container">
                <div className={styles.grid}>
                    <div className={styles.gridItems}>
                        <span>CAPABILITIES</span>
                    </div>
                    <div className={styles.gridItems}>
                        <div className={styles.subGrid}>
                            <div className={styles.subGridItems}>
                                <img src={StrategyIcon} alt="StrategyIcon"/>
                                <h1>Strategy</h1>
                                <p>
                                    Proin sodales nisl vitae dolor dictum, 
                                    non luctus est varius. Fusce ex dolor,
                                </p>
                            </div>
                            <div className={styles.subGridItems}>
                                <img src={DesignIcon} alt="DesignIcon"/>
                                <h1>Design</h1>
                                <p>
                                    Proin sodales nisl vitae dolor dictum, 
                                    non luctus est varius. Fusce ex dolor,
                                </p>
                            </div>
                            <div className={styles.subGridItems}>
                                <img src={DevlopmentIcon} alt="DevlopmentIcon"/>
                                <h1>Development</h1>
                                <p>
                                    Proin sodales nisl vitae dolor dictum, 
                                    non luctus est varius. Fusce ex dolor,
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div className={styles.specializingSection}>
            <div className='container'>
                <div className={styles.grid}>
                    <div className={styles.gridItems}></div>
                    <div className={styles.gridItems}>
                        <h2>
                            Specializing in digital products and <span> process optimization 
                            for real estate,</span>  fintech and medtech companies
                        </h2>
                        <p>
                            We believe that being ambitious with digital technology can be inspirational,
                            but it can also be a challenge. We are here to help you to turn your
                            digital vision into reality and to work intensively with you to make sure that you can implement your vision. 
                        </p>
                        <p>
                            {"That's why we work in a highly iterative and agile way, so that we can deliver on what we promise."}
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
  )
}
