import React from 'react'
const RightIcon = "/assets/icons/right.svg";
const WorkImage = "/assets/images/work.png";
const DesignWorkImage = "/assets/images/design-work.png";
const LrcWorkImage = "/assets/images/sec-work.png";
const VonderWorkImage = "/assets/images/vonder-work.png";
import styles from './our-work.module.scss';
export default function OurWork() {
  return (
    <div>
      <div className='container'>
        <div className={styles.ourWorkAlign}>
          <div className={styles.textGrid}>
            <div className={styles.textGridItems}>
              <span>our work</span>
              <h1>Featured works</h1>
            </div>
            <div className={styles.textGridItems}>
              <p>
                Our recent works that 
                we are proud of
              </p>
              <div className={styles.seeAll}>
                <a>See all</a>
                <img src={RightIcon} alt="RightIcon"/>
              </div>
            </div>
          </div>
          <div className={styles.grid}>
            <div className={styles.gridItems}>
              <div className={styles.cardImage}>
                <img src={WorkImage} alt="WorkImage"/>
              </div>
              <div className={styles.cardDetails}>
                <h3>Healios platform and branding</h3>
                <p>Branding, UI/UX , Website , Mobile app, Development</p>
              </div>
            </div>
            <div className={styles.gridItems}>
              <div className={styles.cardImage}>
                <img src={DesignWorkImage} alt="DesignWorkImage"/>
              </div>
              <div className={styles.cardDetails}>
                <h3>MJHudson ESG Advantage</h3>
                <p>UI/UX , Product design</p>
              </div>
            </div>
            <div className={styles.gridItems}>
              <div className={styles.cardImage}>
                <img src={LrcWorkImage} alt="LrcWorkImage"/>
              </div>
              <div className={styles.cardDetails}>
                <h3>LRC Group Corporate</h3>
                <p>Branding, UI/UX , Website , Mobile app, Development</p>
              </div>
            </div>
            <div className={styles.gridItems}>
              <div className={styles.cardImage}>
                <img src={VonderWorkImage} alt="VonderWorkImage"/>
              </div>
              <div className={styles.cardDetails}>
                <h3>Vonder hotels</h3>
                <p>UI/UX , Website</p>
              </div>
            </div>
          </div>
          <div className={styles.workButtonHidden}>
            <div className={styles.mobileViewButton}>
              <button>
                <span>Case studies</span>
                <img src={RightIcon} alt="RightIcon"/>
              </button> 
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}
