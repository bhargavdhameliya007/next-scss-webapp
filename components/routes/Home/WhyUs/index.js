import React from 'react'
import styles from './why-us.module.scss';
const DownArrow = "/assets/icons/down-arrow.svg";
const ProjectImage = "/assets/images/prjoect-image.png";
export default function WhyUs() {
  return (
    <div>
        <div className={styles.whyUsSection}>
            <div className='container'>
                <div className={styles.textStyle}>
                    <span>WHY US</span>
                    <h1>Top tier people and a <a>methodology that works</a>
                    </h1>
                </div>
                <div className={styles.whyUs}>
                    <div className={styles.card}>
                        <div className={styles.grid}>
                            <div className={styles.gridItems}>
                                <div className={styles.iconTextAlignment}>
                                    <div>
                                        <img src={DownArrow} alt="DownArrow"/>
                                    </div>
                                    <div>
                                        <span>Technologies</span>
                                    </div>
                                </div>
                            </div>
                            <div className={styles.gridItems}>
                                <p>Sed eu magna tellus. Sed hendrerit velit sit amet nisl luctus, at maximus elit facilisis.</p>
                            </div>
                        </div>
                    </div>
                    <div className={styles.card}>
                        <div className={styles.grid}>
                            <div className={styles.gridItems}>
                                <div className={styles.iconTextAlignment}>
                                    <div>
                                        <img src={DownArrow} alt="DownArrow"/>
                                    </div>
                                    <div>
                                        <span>Technologies</span>
                                    </div>
                                </div>
                            </div>
                            <div className={styles.gridItems}>
                                <p>Sed eu magna tellus. Sed hendrerit velit sit amet nisl luctus, at maximus elit facilisis.</p>
                            </div>
                        </div>
                    </div>
                    <div className={styles.card}>
                        <div className={styles.grid}>
                            <div className={styles.gridItems}>
                                <div className={styles.iconTextAlignment}>
                                    <div>
                                        <img src={DownArrow} alt="DownArrow"/>
                                    </div>
                                    <div>
                                        <span>Technologies</span>
                                    </div>
                                </div>
                            </div>
                            <div className={styles.gridItems}>
                                <p>Sed eu magna tellus. Sed hendrerit velit sit amet nisl luctus, at maximus elit facilisis.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div className={styles.allProjectImageShow}>
        </div>
    </div>
  )
}
