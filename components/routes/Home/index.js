import React from 'react'
import Capabilities from './Capabilities';
import HeroBanner from './HeroBanner/index';
import OurWork from './OurWork';
import './home.module.scss';
import ProjectDetails from './ProjectDetails';
import SpecialOps from './SpecialOps';
import WhyUs from './WhyUs';
import News from './News';
export default function Home() {
  return (
    <div>
      <>
        <HeroBanner/>
      </>
      <>
        <SpecialOps/>
      </>
      <>
        <ProjectDetails/>
      </>
      <>
        <Capabilities/>
      </>

      <>
        <OurWork/>
      </>
      <>
        <WhyUs/>
      </>
      <>
        <News/>
      </>
    </div>
  )
}
