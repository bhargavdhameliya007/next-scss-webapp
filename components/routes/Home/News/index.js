import React from 'react'
import styles from './news-blog.module.scss'
const RightIcon = "/assets/icons/right.svg";

export default function News() {
  return (
    <div>
        <div className={styles.newSection}>
            <div className='container'>
                <div className={styles.newSectionAlign}>
                    <div className={styles.newsDetails}>
                        <div className={styles.pageTitle}>
                            <h1>News and blogs</h1>
                        </div>
                        <div className={styles.allnewsShow}>
                            <div className={styles.grid}>
                                <div className={styles.gridItems}>
                                    <h2>We plessed collaborate with companies all size</h2>
                                    <div className={styles.blogview}>
                                        <a>Read</a>
                                        <img src={RightIcon} alt="RightIcon"/>
                                    </div>
                                </div>
                                <div className={styles.gridItems}>
                                    <span>AUG 17 , 2021</span>
                                </div>
                            </div>
                            <div className={styles.grid}>
                                <div className={styles.gridItems}>
                                    <h2>We plessed collaborate with companies all size</h2>
                                    <div className={styles.blogview}>
                                        <a>Read</a>
                                        <img src={RightIcon} alt="RightIcon"/>
                                    </div>
                                </div>
                                <div className={styles.gridItems}>
                                    <span>AUG 17 , 2021</span>
                                </div>
                            </div>
                            <div className={styles.grid}>
                                <div className={styles.gridItems}>
                                    <h2>We plessed collaborate with companies all size</h2>
                                    <div className={styles.blogview}>
                                        <a>Read</a>
                                        <img src={RightIcon} alt="RightIcon"/>
                                    </div>
                                </div>
                                <div className={styles.gridItems}>
                                    <span>AUG 17 , 2021</span>
                                </div>
                            </div>
                        </div>
                        <div className={styles.exploreButton}>
                            <button>
                                <span>Explore stories</span>
                                <img src={RightIcon} alt="RightIcon"/>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  )
}
