import React from 'react'
import styles from './special-ops.module.scss';
const SpecialOpsImage = "/assets/images/special-ops.png";
export default function SpecialOps() {
  return (
    <div>
      <div className='container'>
        <div className={styles.specialOps}>
          <div className={styles.grid}>
            <div className={styles.gridItems}>
              <span>SPECIAL OPS</span>
              <h1>
                We simplify the complex - 
                we’re disciplined, fast and passionate.
              </h1>
            </div>
            <div className={styles.gridItems}>
              <div className={styles.childImage}>
                <img src={SpecialOpsImage} alt="SpecialOpsImage"/>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}
