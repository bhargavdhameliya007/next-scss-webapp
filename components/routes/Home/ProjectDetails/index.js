import React from 'react';
import styles from './project-details.module.scss';
const VonderImage = "/assets/images/vonder.svg";
const MjSudsonImage = "/assets/images/mj.svg";
const PuurspanjeImage = "/assets/images/puurspanje.svg";
const AcMedicalImage = "/assets/images/ac-medical.svg";
const LrcGroupImage = "/assets/images/lrc-group.svg";
export default function ProjectDetails() {
  return (
    <div>
      <div className={styles.ProjectDetailsAlign}>
        <div className='container'>
          <div className={styles.imageFlex}>
            <img src={VonderImage} att="VonderImage"/> 
            <img src={MjSudsonImage} att="MjSudsonImage"/> 
            <img src={PuurspanjeImage} att="PuurspanjeImage"/> 
            <img src={AcMedicalImage} att="AcMedicalImage"/> 
            <img src={LrcGroupImage} att="LrcGroupImage"/> 
          </div>
        </div>
      </div>
    </div>
  )
}
