// import '../styles/globals.css'
import '../styles/mixins/global.scss';
import '../styles/style.css';
import '../styles/theme.css';
import { Provider } from 'jotai';
function MyApp({ Component, pageProps }) 
{
  return   (<><Provider><Component {...pageProps} /></Provider></>)
}

export default MyApp
