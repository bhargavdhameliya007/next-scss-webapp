import Head from 'next/head'
import Image from 'next/image'
import App from '../components/App/index';
import HomePage from '../components/routes/Home/index';
export default function Home() {
  return (
    <>
      <App>
          <HomePage/>
      </App>
    </>
  )
}
